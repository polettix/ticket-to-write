# Ticket to Write - A Roll-and-write game

Welcome to the Ticket to Write repository, with all stuff about Ticket
to Write.

The game is a *roll-and-write* game played on a few sheets of printed
paper, with pen/pencils and 7 dice. You can of course laminate the
sheets and use whiteboard markers instead of pencils. Or use beans as
markers instead of scribbling. I mean, you can get creative!

The experience I'd like to re-create is that of one game from the
[Ticket to Ride][ttr] series. In particular, the specific board and
tickets generated in the first installment are heavily influenced by
[Ticket to Ride London][ttrl], except that the artwork is completely
redone, the tickets have been generated ex-novo with the help of the
`ttw` program in the repository and... it uses dice instead of cards!

PDF files to print and play can be found in the [Package Registry][pr]
of this repository.

## ACKNOWLEDGEMENTS

Thanks to:

- [Ticket to Ride][ttr] and [Ticket to Ride London][ttrl] for the
  inspiration provided to this game. They are both games with their own
  market and intellectual property, which are not reused here.
- Font [Some Time Later][stl] is used extensively and is awesome
  ([SIL OFL][silofl])
- Font [PT Sans][ptsans] is used in the instructions and is awesome as
  well ([SIL OFL][silofl])
- [Dice images][dice] ([CC0][cc0])
- [Pencil icon][pencil] ([CC0][cc0])
- [InkScape][is] ([GPL 2][gpl2])
- [OpenOffice][ooo] ([Apache 2.0][apache-2.0])

## COPYRIGHT AND LICENSE

The game is inspired to the Ticket to Ride saga, by Alan R. Moon. This
is an ode to this wonderful series of games, anything having to do with
the original game is copyright by the respective owners.

The stuff in this repository is Copyright 2022 by Flavio Poletti.

The documentation (non-code) parts for Ticket to Write inside this
repository are CC0:

> To the extent possible under law, the person who associated CC0 with
> Ticket to Write SVG documents (non-code) has waived all
> copyright and related or neighboring rights to Ticket to Write
> documents (non-code).
>
> You should have received a copy of the CC0 legalcode along with this
> work.  If not, see
> [http://creativecommons.org/publicdomain/zero/1.0/](http://creativecommons.org/publicdomain/zero/1.0/).

The code parts in the repository are Apache 2.0:

> Licensed under the Apache License, Version 2.0 (the "License"); you
> may not use this file except in compliance with the License. You may
> obtain a copy of the License at
>
>> [http://www.apache.org/licenses/LICENSE-2.0][apache-2.0]
>
> or look for file `LICENSE` in this project's root directory.
>
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
> implied. See the License for the specific language governing
> permissions and limitations under the License.

[cc0]: http://creativecommons.org/publicdomain/zero/1.0/
[apache-2.0]: http://www.apache.org/licenses/LICENSE-2.0
[ttr]: https://boardgamegeek.com/boardgame/9209/ticket-ride
[ttrl]: https://boardgamegeek.com/boardgame/276894/ticket-ride-london
[pr]: https://gitlab.com/polettix/ticket-to-write/-/packages
[gpl2]: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
[silofl]: https://scripts.sil.org/OFL
[stl]: https://github.com/ctrlcctrlv/some-time-later
[oca]: https://openclipart.org/
[dice]: https://openclipart.org/detail/105931/sixsided-dice-faces-lio-01
[pencil]: https://openclipart.org/detail/24821/pencil-icon
[is]: https://inkscape.org/
[ooo]: https://www.openoffice.org/it/
[ptsans]: https://www.paratype.ru/fonts/pt/pt-sans
